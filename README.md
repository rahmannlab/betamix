# BetaMix

Original version by Christopher Schröder and Sven Rahmann.\
Maintained by Saim Momin.

Please cite:

Christopher Schröder and Sven Rahmann:
**A hybrid parameter estimation algorithm for beta mixtures and applications to methylation state classification.**
*Algorithms Mol Biol* 12:21 (2017 Aug 18);
doi: `10.1186/s13015-017-0112-1`.

All data from the paper is contained in the `data/` directory.
A workflow that reproduces our results is contained in the `Snakefile`, which can be run by Snakemake.
All code and data is contained herein.
To reproduce everything from scratch, follow the steps described in workflow.txt

If you have any questions, contact Sven.Rahmann [-@-] uni-saarland [-.-] de.

