from itertools import count
from argparse import ArgumentParser

import numpy as np
import scipy as sp
from numpy.random import random
from scipy.stats import beta
import h5py


def get_random_beta_parameters(shape, unimodalfactor=5.0):
    """
    Return random parameters (a, b) for a beta distribution,
    such that the distribution obeys the given shape.
    If shape=="falling", chose 0<a<=1 and b>1
    If shape=="rising", choose a>1 and 0<b<=1
    If shape=="unimodal", choose a>1 and b>1
    If shape=="bimodal", choose 0<a<=1 and 0<b<=1
    Other shapes raise KeyError.
    Values between 0 and 1 are drawn uniformly.
    Values above 1 are drawn uniformly as x, then 1/x is applied.
    """
    unimodal = (shape == "unimodal")
    (fa, fb) = _f_ab[shape]
    (a, b) = (fa(), fb())
    if unimodal:
        mu = unimodalfactor * max(a,b)
        u = np.random.uniform(0.9, 1.1)
        a = mu/u
        b = mu*u
    return (a, b)

def _small():
    x=random()
    return x if x!=0.0 else 1.0

def _big():
    x=random()
    while x==0.0:  x=random()
    return 1/x

_f_ab = dict( falling=(_small,_big), rising=(_big,_small), 
              unimodal=(_big,_big), bimodal=(_small,_small)
            )


def get_mixture_dataset(components, n):
    """
    Given a list of component types and a desired number n of samples,
    return triple (ab, pi, data), where
    - ab is the list of (alpha, beta) parameters for each component,
    - pi is the array of mixture coefficients,
    - data is the sorted vector of (approximately) n samples.
    """
    if len(components) == 0:
        raise ValueError("need at least one component")
    ab = [get_random_beta_parameters(c) for c in components]  # list of parameters
    C = len(components)
    while True:
        pi = random(C)
        if min(pi) > 0:  break
    pi /= sum(pi)
    sizes = np.array(np.round(n*pi), dtype=int)
    N = int(np.sum(sizes))
    data = np.zeros(N, dtype=float)
    origins = np.full(N, -1, dtype=np.int32)
    start = 0
    for (c, size, (a,b)) in zip(count(), sizes, ab):
        x = beta.rvs(a, b, size=size).tolist()
        data[start:start+size] = x
        origins[start:start+size] = c
        start += size
    # find permutation that sorts the data
    perm = np.argsort(data)
    # sort data and origins according to permutation
    return (ab, pi, data[perm], origins[perm])


def adjust_data(data, n0, n1):
    n = len(data)
    if n0 < 0 or n1 < 0:
        raise ValueError("n0, n1 must be non-negative")
    if n0+n1 > n:
        raise ValueError("n = {} < {} + {} = n0 + n1".format(n,n0,n1))
    if n0>0:  data[:n0] = 0.0
    if n1>0:  data[-n1:] = 1.0


def get_argument_parser():
    p = ArgumentParser(description="Simulation of beta mixtures")
    p.add_argument("--seed", type=int,
        help="random seed to use for simulation")
    p.add_argument("--datasets", "-m", type=int, default=1,
        help="number of datasets to simulate")
    p.add_argument("--first", type=int, default=0,
        help="number of first datset [0]")
    p.add_argument("--samples", "-n", type=int, default=1000,
        help="number of samples per dataset")
    p.add_argument("--zeros", "-0", type=int, default=0,
        help="number of samples to set to 0.0")
    p.add_argument("--ones", "-1", type=int, default=0,
        help="number of samples to set to 1.0")
    p.add_argument("--components", nargs="+", metavar="COMPONENT", 
        choices=("falling", "rising", "unimodal", "bimodal"),
        default=["falling", "unimodal", "rising"],
        help="sequence of components for beta mixture")
    p.add_argument("outpath",
        help="path to output file (should be a HDF5 file)")
    return p


def main():
    p = get_argument_parser()
    args = p.parse_args()
    seed = args.seed
    if seed is not None:
        np.random.seed(seed)
    m = args.datasets  # number of datasets to simulate
    mstart = args.first 
    n = args.samples  # number of points per dataset
    n0 = args.zeros
    n1 = args.ones
    components = args.components
    outpath = args.outpath
    with h5py.File(outpath) as f:
        for t in range(mstart, mstart+m):
            # generate dataset t
            (ab, pi, data, origins) = get_mixture_dataset(components, n)
            adjust_data(data, n0, n1)
            # Write data to output file
            gdata = f.require_group("data")
            gtruth = f.require_group("truth")
            dname = str(t)
            if dname in gdata:  del gdata[dname]
            if dname in gtruth:  del gtruth[dname]
            gdata.create_dataset(dname, data=data)
            gtruth.create_dataset(dname+"/ab", data=np.array(ab, dtype=float))
            gtruth.create_dataset(dname+"/pi", data=pi)
            gtruth.create_dataset(dname+"/c", data=origins)


if __name__ == "__main__":
    main()


