
from itertools import count
from argparse import ArgumentParser
import os
import os.path

import numpy as np
from numba import njit
from scipy.stats import beta
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
from h5py import File as H5File


def get_argument_parser():
    p = ArgumentParser(description="Evaluation of beta mixtures")
    p.add_argument("--datasets", "-d", nargs="+", metavar="DATASET",
        help="name(s) of dataset(s) in HDF5 file to process")
    p.add_argument("--imagepath", "-p", "-i", metavar="IMAGEPATH",
        help="path to save images to (omit for no images)")
    p.add_argument("--force", "--overwrite", "-F", action="store_true",
        help="overwrite an existing evaluation")
    p.add_argument("--format", "-f", default="png",
        help="output format for images [png]")
    p.add_argument("--dpi", "-D", type=int, default=200,
        help="dpi for output images")
    p.add_argument("--bins", "-B", type=int,
        help="number of bins for plotting histograms")
    p.add_argument("--densitypoints", "-P", type=int, default=1024,
        help="number of evaluation points for plotting densities")
    p.add_argument("datapath", metavar="DATA_HDF5",
        help="path to HDF5 file with datasets and estimates")
    p.add_argument("estimatepath", metavar="ESTIMATE_HDF5",
        help="path to HDF5 fie with estimated parameters")
    p.add_argument("evalpath", metavar="EVAL_HDF5",
        help="outputpath to HDF5 file with evaluation") 
    return p


def ensure_directory(d):
    """
    Ensure that directory <d> exists.
    It is an error to pass anything else than a directory string.
    """
    if d == "": return
    d = os.path.abspath(d)
    os.makedirs(d, exist_ok=True)


def get_density(ab, pi, resolution):
    nc = len(ab)
    x = np.linspace(0.0, 1.0, resolution)
    y = np.zeros_like(x)
    ys = np.zeros((nc, len(x)), dtype=np.float64)
    for (j,p,(a,b)) in zip(count(),pi,ab):
        z = p * beta.pdf(x, a, b)
        ys[j,:] = z
        y += z
    return x, y, ys


def _compute_auc(xs, ids, xx, ys, a):
    n = len(xs)
    for (x, i, j) in zip(xx, ids, count()):
        if i < n and x == xs[i]:
            aa = ys[i]
        elif i == 0:
            assert x < xs[i]
            aa = ys[0] * x/xs[0]
        elif i == n:
            assert x > xs[n-1]
            aa = ys[n-1]
        else:
            assert xs[i-1] < x < xs[i]
            wi = (x - xs[i-1]) / (xs[i] - xs[i-1])
            assert 0 < wi <= 1
            aa = ys[i]*wi + ys[i-1]*(1.0-wi)
        a[j] = aa


def area_between_curves(xs1, ys1, xs2, ys2, points):
    """return are between curves (0,0)->(xs1, ys1) and (0,0)->(xs2, ys2)"""
    xs1, id1 = np.unique(xs1, return_index=True)
    ys1 = ys1[id1]
    xs2, id2 = np.unique(xs2, return_index=True)
    ys2 = ys2[id2]
    xx = np.linspace(0.0, 1.0, points)
    id1 = np.searchsorted(xs1, xx)  # find indices of x in xs1
    id2 = np.searchsorted(xs2, xx)  # find indices of x in xs2
    a1 = np.zeros_like(xx)
    a2 = np.zeros_like(xx)
    _compute_auc(xs1, id1, xx, ys1, a1)
    _compute_auc(xs2, id2, xx, ys2, a2)
    abc = np.sum(a1-a2) / (points-1)
    return abc, xx, a1, a2


def best_correct(classes):
    return 0.0


def evaluate(fdata, fest, feval, d, args):
    """
    in HDF5 file f, evaluate dataset d:
    - write an image to show the true and estimated distribution
    - classify datapoints into classes
    """
    ipath = args.imagepath
    res = args.densitypoints
    dpi = args.dpi
    gdata = fdata['data']  # the group with datasets
    gest = fest['estimation']
    gtruth = fdata['truth'] if 'truth' in fdata else dict()
    geval = feval['evaluation']
    overwrite = args.force
    if (d in geval) and not overwrite:
        print("{}: skipping".format(d))
        return None, None

    x = gdata[d][:]
    ab = gest[d]["ab"][:]
    pi = gest[d]["pi"][:]
    w = gest[d]["w"][:]

    truth = d in gtruth
    if truth:
        abt = gtruth[d]["ab"][:]
        pit = gtruth[d]["pi"][:]
        clt = gtruth[d]["c"][:]
    else:
        abt = pit = clt = None

    # plot data, estimated density and perhaps true density
    bins = args.bins  if args.bins is not None  else max(2, len(x)//10)
    if ipath is not None:
        outpath = ipath + '/' + d + '.' + args.format
        sns.distplot(x, kde=False, bins=bins, norm_hist=True)
        if truth:
            pts, yt, _ = get_density(abt, pit, res)
            plt.plot(pts, yt, 'g-', label='true')
        pts, y, ys = get_density(ab, pi, res)
        plt.plot(pts, y, 'b-', label="estimated")
        for j, yc in enumerate(ys):
            plt.plot(pts, yc, '--', label="component {}".format(j))
        plt.xlim([0,1])
        print("{}: mixture plot -> {}".format(d, outpath))
        plt.savefig(outpath, bbox_inches='tight', dpi=dpi)
        plt.close()

    # infer a class for each x from w
    if d in geval:
        del geval[d]
    n = len(x)
    nc = len(pi)
    cl = w.argmax(1)  # classes from mixture model
    mids = np.tile(np.linspace(0.0, 1.0, nc), (n,1))
    dist = np.abs(x.reshape(n,1) - mids)
    cl0 = dist.argmin(1)  # trivial rule
    geval.create_dataset(d+"/mix/classes", data=cl)
    geval.create_dataset(d+"/ncc/classes", data=cl0)

    if clt is None:
        return None, None

    # if we know the true classes, compute a ROC curve.
    equal = cl==clt
    mw = np.amax(w, 1)
    correct = np.zeros(100, dtype=float)
    valid = np.zeros(100, dtype=float)
    for i,t in enumerate(np.linspace(1/nc, 1.0, 100)):
        val = mw >= t
        correct[i] = np.sum(np.logical_and(equal, val))/n
        valid[i] = np.sum(val)/n
    geval.create_dataset(d+"/hmw/correct", data=correct)
    geval.create_dataset(d+"/hmw/valid", data=valid)

    wsorted = np.sort(w, axis=1)
    gap = wsorted[:,-1] - wsorted[:,-2]
    gcorrect = np.zeros(100, dtype=float)
    gvalid = np.zeros(100, dtype=float)
    for i,t in enumerate(np.linspace(0.0, 1.0, 100)):
        val = gap >= t
        gcorrect[i] = np.sum(np.logical_and(equal, val))/n
        gvalid[i] = np.sum(val)/n
    geval.create_dataset(d+"/gmw/correct", data=gcorrect)
    geval.create_dataset(d+"/gmw/valid", data=gvalid)
    

    equal = cl0==clt
    mdist = np.amin(dist, 1)
    trivcorrect = np.zeros(100, dtype=float)
    trivvalid = np.zeros(100, dtype=float)
    for i,t in enumerate(np.linspace(0.0, 1/(nc+1), 100)):
        val = mdist <= t
        trivcorrect[i] = np.sum(np.logical_and(equal, val))/n
        trivvalid[i] = np.sum(val)/n
    geval.create_dataset(d+"/ncc/correct", data=trivcorrect)
    geval.create_dataset(d+"/ncc/valid", data=trivvalid)

    abc, xx, a1, a2 = area_between_curves(valid, correct, trivvalid, trivcorrect, res)
    dbc = a1[-1] - a2[-1]
    geval.create_dataset(d+"/abc", data=abc)
    geval.create_dataset(d+"/dbc", data=dbc)
    best = best_correct(clt)
    if ipath is not None:
        outpath = ipath + '/' + d + ".roc." + args.format
        plt.plot(gvalid, gcorrect, 'm-', label='high gap max weight')
        plt.plot(gvalid, gcorrect/gvalid, 'm--', label='high gap max weight')
        plt.plot(valid, correct, 'r-', label='high max weight')
        plt.plot(valid, correct/valid, 'r--', label='high max weight')
        plt.plot(trivvalid, trivcorrect, 'b-', label='fixed')
        plt.plot(trivvalid, trivcorrect/trivvalid, 'b--', label='fixed')
        plt.plot(xx, xx, 'k-', alpha=0.5, label='diagonal')
        ##plt.plot(xx, np.full_like(xx, best), 'k--', alpha=0.5, label='optimal')
        plt.axis([0,1,0,1])
        plt.xlabel('fraction of called classes')
        plt.ylabel('fraction correct;  precision', fontsize='small')
        #plt.legend(loc='center left')
        plt.savefig(outpath, bbox_inches='tight', dpi=dpi)
        plt.close()
    return abc, a1[-1]-a2[-1]


def main():
    p = get_argument_parser()
    args = p.parse_args()
    ipath = args.imagepath
    if ipath is not None:
        ensure_directory(ipath)
        sns.set(font_scale=2.5)
        sns.set_style("whitegrid")
        
    with  H5File(args.datapath, "r") as fdata, \
          H5File(args.estimatepath, "r") as fest, \
          H5File(args.evalpath) as feval:
        ds = args.datasets  if args.datasets is not None  else list(fest['estimation'].keys())
        feval.require_group('evaluation')
        for d in ds:
            abc, dbc = evaluate(fdata, fest, feval, d, args)


if __name__ == "__main__":
    main()


