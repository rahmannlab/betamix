from itertools import count
from argparse import ArgumentParser

import numpy as np
from scipy.stats import beta
import h5py


def deprecated(func):
    """a decorator that does nothing"""
    return func


def _get_values(x, left, right):
    y = x[np.logical_and(x>=left, x<=right)]
    n = len(y)
    if n == 0:
        m = (left+right) / 2.0
        v = (right-left) / 12.0
    else:
        m = np.mean(y)
        v = np.var(y)
        if v == 0.0:
            v = (right-left) / (12.0*(n+1))
    return m, v, n

def get_initialization(x, ncomponents, limit=0.8):
    # TODO: work with specific components instead of just their number
    points = np.linspace(0.0, 1.0, ncomponents+2)
    means = np.zeros(ncomponents)
    variances = np.zeros(ncomponents)
    pi = np.zeros(ncomponents)
    # init first component
    means[0], variances[0], pi[0] = _get_values(x, points[0], points[1])
    # init intermediate components
    N = ncomponents - 1
    for j in range(1, N):
        means[j], variances[j], pi[j] = _get_values(x, points[j], points[j+2])
    # init last component
    means[N], variances[N], pi[N] = _get_values(x, points[N+1], points[N+2])
    
    # compute parameters ab, pi
    ab = [ab_from_mv(m,v) for (m,v) in zip(means,variances)]
    pi = pi / pi.sum()
    
    # adjust first and last
    if ab[0][0] >= limit:  ab[0] = (limit, ab[0][1])
    if ab[-1][1] >= limit:  ab[-1] = (ab[-1][0], limit)
    return ab, pi


def ab_from_mv(m, v):
    """
    estimate beta parameters (a,b) from given mean and variance;
    return (a,b).

    Note, for uniform distribution on [0,1], (m,v)=(0.5,1/12)
    """
    phi = m*(1-m)/v - 1  # z = 2 for uniform distribution
    return (phi*m, phi*(1-m))  # a = b = 1 for uniform distribution


def get_weights(x, ab, pi):
    """return nsamples X ncomponents matrix with association weights"""
    bpdf = beta.pdf
    n, c = len(x), len(ab)
    y = np.zeros((n,c), dtype=float)
    s = np.zeros((n,1), dtype=float)
    for (j, p,(a,b)) in zip(count(), pi, ab):
        y[:,j] = p * bpdf(x, a, b)
    s = np.sum(y,1).reshape((n,1))
    np.seterr(divide='ignore', invalid='ignore')
    w = y / s  # this may produce inf or nan; this is o.k.!
    # clean up weights w, remove infs, nans, etc.
    wfirst = np.array([1] + [0]*(c-1), dtype=float)
    wlast = np.array([0]*(c-1) + [1], dtype=float)
    bad = (~np.isfinite(w)).any(axis=1)
    badfirst = np.logical_and(bad, x<0.5)
    badlast = np.logical_and(bad, x>=0.5)
    w[badfirst,:] = wfirst
    w[badlast,:] = wlast
    # now all weights are valid finite values and sum to 1 for each row
    assert np.all(np.isfinite(w)), (w, np.isfinite(w))
    assert np.allclose(np.sum(w,1), 1.0),  np.max(np.abs(np.sum(w,1)-1.0))
    return w


def relerror(x,y):
    if x==y:  return 0.0
    return abs(x-y)/max(abs(x),abs(y))

def get_delta(ab, abold, pi, piold):
    epi = max(relerror(p,po) for (p,po) in zip(pi,piold))
    ea =  max(relerror(a,ao) for (a,_), (ao,_) in zip(ab,abold))
    eb =  max(relerror(b,bo) for (_,b), (_,bo) in zip(ab,abold))
    return max(epi,ea,eb)


def estimate_mixture(x, init, steps=1000, tolerance=1E-5):
    """
    estimate a beta mixture model from the given data x
    with the given number of components and component types
    """
    (ab, pi) = init
    n, ncomponents = len(x), len(ab)

    for step in count():
        if step >= steps:  
            break
        abold = list(ab)
        piold = pi[:]
        # E-step: compute component memberships for each x
        w = get_weights(x, ab, pi)
        # compute component means and variances and parameters
        for j in range(ncomponents):
            wj = w[:,j]
            pij = np.sum(wj)
            m = np.dot(wj,x) / pij
            v = np.dot(wj,(x-m)**2) / pij
            if np.isnan(m) or np.isnan(v):
                m = 0.5;  v = 1/12  # uniform
                ab[j]=(1,1)  # uniform
                assert pij == 0.0
            else:
                assert np.isfinite(m) and np.isfinite(v), (j,m,v,pij)
                ab[j] = ab_from_mv(m,v)
            pi[j] = pij / n
        delta = get_delta(ab, abold, pi, piold)
        if delta < tolerance:
            break
    usedsteps = step + 1
    return (ab, pi, usedsteps)


def estimate(x, components, steps=1000, tolerance=1E-4):
    init = get_initialization(x, len(components))
    (ab, pi, usedsteps) = estimate_mixture(x, init, steps=steps, tolerance=tolerance)
    return (ab, pi, usedsteps)


def get_argument_parser():
    p = ArgumentParser(description="Estimation of beta mixtures")
    p.add_argument("--datasets", nargs="+", metavar="DATASET",
        help="name(s) of dataset(s) in HDF5 file to process")
    p.add_argument("--list", "-l", action="store_true",
        help="only list datsets and their sizes, do not estimate")
    p.add_argument("--force", "--overwrite", "-F", action="store_true",
        help="overwrite an existing estimation")
    p.add_argument("--maxiter", "-I", type=int, default=1000,
        help="maximum number of iterations")
    p.add_argument("--tolerance", "-t", type=float, default=1E-4,
        help="convergence criterion [1E-4]")
    p.add_argument("--components", "-c", nargs="+", metavar="COMPONENT",
        default=["falling", "unimodal", "rising"],
        choices=("falling", "rising", "unimodal", "bimodal"),
        help="sequence of components for beta mixture")
    p.add_argument("datapath", metavar="DATA",
        help="path of HDF5 file with datasets or text input of single column of methylation values (requires --text)")
    p.add_argument("--resultpath", metavar="RESULT_H5",
        help="path of HDF5 file where to write estimated parameters")
    p.add_argument("--text", action="store_true",
        help="use text input and output")
    return p


def main():
    p = get_argument_parser()
    args = p.parse_args()
    steps = args.maxiter
    tolerance = args.tolerance
    overwrite = args.force
    listonly = args.list
    if args.text: # text in and output
        x = np.loadtxt(args.datapath)
        (ab, pi, us) = estimate(x, args.components, steps=steps, tolerance=tolerance)
        w = get_weights(x, ab, pi)

        print("ab", end="\t")
        print(*[f"{j},{k}" for j, k in ab], sep="\t")

        print("pi", end="\t")
        print(*pi, sep=",")

        for i, y in enumerate(w):
            print(f"w{i}", *y, sep="\t")
        print("us", us)
    else:
        with h5py.File(args.datapath, "r") as f, h5py.File(args.resultpath) as fout:
            gest = fout.require_group("estimation")
            gdata = f['data']  # the group with datasets
            ds = args.datasets  if args.datasets is not None else list(gdata.keys())
            for d in ds:
                if (d in gest) and not overwrite and not listonly:
                    print("{}: skipping".format(d))
                    continue
                x = gdata[d][:]
                if listonly:
                    print(d, len(x))
                    continue
                (ab, pi, us) = estimate(x, args.components, steps=steps, tolerance=tolerance)
                msg = "aborting after {} steps".format(steps)  if us>steps  else "converged after {} steps".format(us)
                print("{}: {}".format(d, msg))
                w = get_weights(x, ab, pi)

                # Write estimate to h5 file
                if d in gest:
                    del gest[d]
                gest.create_dataset(d+"/ab", data=np.array(ab, dtype=float))
                gest.create_dataset(d+"/pi", data=pi)
                gest.create_dataset(d+"/w", data=w)

    
if __name__ == "__main__":
    main()
    
